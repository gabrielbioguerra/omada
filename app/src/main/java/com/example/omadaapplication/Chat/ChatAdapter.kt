package com.example.omadaapplication.Match

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.omadaapplication.R

class ChatAdapter(
    _chatList: List<ChatObject>,
    _context: Context
) : RecyclerView.Adapter<ChatViewHolders>() {

    var chatList: List<ChatObject> = _chatList
    var context: Context = _context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolders {

        var layoutView = LayoutInflater.from(parent.context).inflate(R.layout.item_chat, null, false)
        var lp = RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layoutView.layoutParams = lp
        return ChatViewHolders((layoutView))
    }

    override fun getItemCount(): Int {
        return chatList.size
    }

    override fun onBindViewHolder(holder: ChatViewHolders, position: Int) {
        holder.holderMessage.text = chatList[position]._message
        if(chatList[position]._currentUser){
            holder.holderMessage.gravity = Gravity.RIGHT
            holder.holderMessage.setTextColor(Color.parseColor("#404040"))
            holder.holderLayout.setBackgroundColor(Color.parseColor("#F4F4F4"))
        }else {
            holder.holderMessage.gravity = Gravity.LEFT
            holder.holderMessage.setTextColor(Color.parseColor("#FFFFFF"))
            holder.holderLayout.setBackgroundColor(Color.parseColor("#2DB4C8"))
        }
    }
}