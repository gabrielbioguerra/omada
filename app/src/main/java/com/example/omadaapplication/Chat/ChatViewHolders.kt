package com.example.omadaapplication.Match

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.omadaapplication.Chat.Chat
import com.example.omadaapplication.R

class ChatViewHolders(
    _view: View
) : RecyclerView.ViewHolder(_view), View.OnClickListener {

    var view = _view
    var holderLayout = view.findViewById<LinearLayout>(R.id.item_chat_container)
    var holderMessage = view.findViewById<TextView>(R.id.item_chat_message)

    override fun onClick(v: View?) {

    }
}
