package com.example.omadaapplication.Chat

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.omadaapplication.Match.ChatAdapter
import com.example.omadaapplication.Match.ChatObject
import com.example.omadaapplication.Match.ChatViewHolders
import com.example.omadaapplication.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class Chat : AppCompatActivity() {

    var recycler: RecyclerView? = null
    var navigation: BottomNavigationView? = null
    var back: Button? = null

    var chatAdapter: RecyclerView.Adapter<ChatViewHolders>? = null
    var chatLayoutManager: RecyclerView.LayoutManager? = null

    var resultsChat = ArrayList<ChatObject>()

    var userDbUser: DatabaseReference? = null
    var chatDbChat: DatabaseReference? = null
    var currentUserId: String? = null
    var matchId: String? = null

    var sendMessage: EditText? = null
    var sendButton: Button? = null

    var chatId: String? = null

    var firebaseAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        matchId = intent.extras.getString("matchId")

        firebaseAuth = FirebaseAuth(FirebaseApp.getInstance())
        firebaseAuth?.addAuthStateListener { user ->
            var auth = user.currentUser
            if(auth != null){
                currentUserId = auth.uid
            }else
            {

            }
        }
        currentUserId = FirebaseAuth.getInstance().currentUser?.uid

        userDbUser = FirebaseDatabase.getInstance().reference.child("User").child(currentUserId!!).child("Connections")
            .child("Matches").child(matchId!!).child("ChatId")

        chatDbChat = FirebaseDatabase.getInstance().reference.child("Chat")

        getChatId()

        sendMessage = findViewById(R.id.chat_edt_message)
        sendButton = findViewById(R.id.chat_btn_send)

        back = findViewById(R.id.chat_btn_back)
        back?.setOnClickListener {
            onBackPressed()
        }

        recycler = findViewById(R.id.chat_recycler)
        recycler?.isNestedScrollingEnabled = false
        recycler?.setHasFixedSize(false)
        chatLayoutManager = LinearLayoutManager(this@Chat)
        recycler?.layoutManager = chatLayoutManager
        chatAdapter = ChatAdapter(resultsChat as List<ChatObject>, this@Chat)
        recycler?.adapter = chatAdapter
        chatAdapter?.notifyDataSetChanged()

        sendButton?.setOnClickListener {
            sendMessages()
        }
    }

    private fun sendMessages() {
        var message = sendMessage?.text.toString()
        if (message.isNotEmpty()) {
            var messageDb = chatDbChat?.push()
            var newMessage = HashMap<String, String>()
            newMessage.put("Creator", currentUserId!!)
            newMessage.put("Text", message)

            messageDb?.setValue(newMessage)
        }
        sendMessage?.text = null
    }

     private fun getChatId() {
        userDbUser?.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    chatId = p0.value.toString()
                    chatDbChat = chatDbChat?.child(chatId!!)
                    getChatMessages()
                }

            }

        })
    }

    private fun getChatMessages() {
        chatDbChat?.addChildEventListener(object: ChildEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {}

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {}

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                if(p0.exists()){
                    var message: String? = null
                    var createdByUser: String? = null

                    if(p0.child("Text").value.toString() != null){
                        message = p0.child("Text").value.toString()
                    }

                    if(p0.child("Creator").value.toString() != null){
                       createdByUser = p0.child("Creator").value.toString()
                    }

                    if(message != null && createdByUser != null) {
                        var currentUserBoolean: Boolean = false
                        if(createdByUser.equals(currentUserId)) {
                            currentUserBoolean = true
                        }
                        var obj = ChatObject(message, currentUserBoolean)
                        resultsChat.add(obj)
                        chatAdapter?.notifyDataSetChanged()
                    }
                }
            }

            override fun onChildRemoved(p0: DataSnapshot) {}

        })
    }
}
