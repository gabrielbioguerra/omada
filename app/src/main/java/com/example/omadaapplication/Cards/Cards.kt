package com.example.omadaapplication.Cards

class Cards(
    userId: String,
    name: String,
    bio: String,
    skill1: String,
    skill2: String,
    skill3: String,
    interest1: String,
    interest2: String,
    interest3: String) {

    var  _userId = userId
    var _name = name
    var _bio = bio
    var _skill1 = skill1
    var _skill2 = skill2
    var _skill3 = skill3
    var _interest1 = interest1
    var _interest2 = interest2
    var _interest3 = interest3

}