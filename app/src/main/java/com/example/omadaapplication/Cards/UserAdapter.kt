package com.example.omadaapplication.Cards

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.omadaapplication.R

class UserAdapter(context: Context, private val resourceId: Int, private val items: MutableList<Cards>) : ArrayAdapter<Cards> (context, resourceId, items){

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        return createViewFromResource(position, convertView, parent)
    }

    fun createViewFromResource(position: Int, convertView: View?, parent: ViewGroup?): View? {

        val view: View = convertView?: LayoutInflater.from(context).inflate(resourceId , parent, false) as View
        //convertView as TextView? ?: LayoutInflater.from(context).inflate(resourceId , parent, false) as View
        //view.text = list[position].name

        var name = view.findViewById<TextView>(R.id.item_name)
        var bio = view.findViewById<TextView>(R.id.item_bio)
        var skill1 = view.findViewById<TextView>(R.id.item_skill01)
        var skill2 = view.findViewById<TextView>(R.id.item_skill02)
        var skill3 = view.findViewById<TextView>(R.id.item_skill03)
        var interest1 = view.findViewById<TextView>(R.id.item_interest01)
        var interest2 = view.findViewById<TextView>(R.id.item_interest02)
        var interest3 = view.findViewById<TextView>(R.id.item_interest03)

        name.text = items[position]._name
        bio.text = items[position]._bio
        skill1.text = items[position]._skill1
        skill2.text = items[position]._skill2
        skill3.text = items[position]._skill3
        interest1.text = items[position]._interest1
        interest2.text = items[position]._interest2
        interest3.text = items[position]._interest3

        var skills = view.findViewById<TextView>(R.id.item_skills)
        skills.text = "Skills"
        var interests = view.findViewById<TextView>(R.id.item_interests)
        interests.text = "Interests"
        var bio_tittle = view.findViewById<TextView>(R.id.item_bio_tittle)
        bio_tittle.text = "Bio"

        return view
    }
}