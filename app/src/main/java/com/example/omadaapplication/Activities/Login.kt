package com.example.omadaapplication.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.omadaapplication.R
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth

class Login : AppCompatActivity() {

    var edtEmail: EditText? = null
    var edtPassword: EditText? = null
    var btnLogin: Button? = null
    var btnBack: TextView? = null

    var firebaseAuth: FirebaseAuth = FirebaseAuth(FirebaseApp.getInstance())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        edtEmail = findViewById(R.id.login_edt_email)
        edtPassword = findViewById(R.id.login_edt_password)
        btnLogin = findViewById(R.id.login_btn_login)
        btnBack = findViewById(R.id.login_txv_back)

        btnBack?.setOnClickListener {
            val intent = Intent(this@Login, Splash::class.java)
            startActivity(intent)
        }

        btnLogin?.setOnClickListener {
            var email = edtEmail?.text.toString()
            var password = edtPassword?.text.toString()

            if (email.isNotEmpty()!! && password.isNotEmpty()!!) {
                firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
                    if (it.isSuccessful) {
                        Log.i("Login", "Successful")
                        val intent = Intent(this@Login, MainActivity::class.java)
                        startActivity(intent)
                    } else {
                        Log.i("Login", "Error")
                    }
                }
                    .addOnFailureListener {
                        Toast.makeText(this, it.toString(), Toast.LENGTH_SHORT).show()
                    }
            } else {
                Toast.makeText(this, "Complete the fields", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
