package com.example.omadaapplication.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.example.omadaapplication.R
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.vicmikhailau.maskededittext.MaskedEditText

class Register : AppCompatActivity() {

    var lyt01: LinearLayout? = null
    var edtBirth: MaskedEditText? = null
    var edtName: EditText? = null
    var spnSex: Spinner? = null
    var btnNext01: Button? = null
    var btnBack01: Button? = null
    var birth: String? = null
    var name: String? = null
    var sex: String? = null

    var lyt02: LinearLayout? = null
    var spnSkill1: Spinner? = null
    var spnSkill2: Spinner? = null
    var spnSkill3: Spinner? = null
    var btnNext02: Button? = null
    var btnBack02: Button? = null
    var skill01: String? = null
    var skill02: String? = null
    var skill03: String? = null

    var lyt03: LinearLayout? = null
    var spnInterest1: Spinner? = null
    var spnInterest2: Spinner? = null
    var spnInterest3: Spinner? = null
    var btnNext03: Button? = null
    var btnBack03: Button? = null
    var interest01: String? = null
    var interest02: String? = null
    var interest03: String? = null

    var lyt04: LinearLayout? = null
    var edtBio: EditText? = null
    var btnNext04: Button? = null
    var btnBack04: Button? = null
    var bio: String? = null

    var lyt05: LinearLayout? = null
    var edtEmail: EditText? = null
    var edtPassword: EditText? = null
    var btnRegister: Button? = null
    var btnBack05: Button? = null

    var firebaseAuth: FirebaseAuth = FirebaseAuth(FirebaseApp.getInstance())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        renderLayouts()

        renderFields()

        renderButtons()

        btnBack01?.setOnClickListener {
            val intent = Intent(this@Register, Splash::class.java)
            startActivity(intent)
        }

        btnBack02?.setOnClickListener {
            lyt02?.visibility = View.GONE
            lyt01?.visibility = View.VISIBLE
        }

        btnBack03?.setOnClickListener {
            lyt03?.visibility = View.GONE
            lyt02?.visibility = View.VISIBLE
        }

        btnBack04?.setOnClickListener {
            lyt04?.visibility = View.GONE
            lyt03?.visibility = View.VISIBLE
        }

        btnBack05?.setOnClickListener {
            lyt05?.visibility = View.GONE
            lyt04?.visibility = View.VISIBLE
        }

        btnNext01?.setOnClickListener {
            name = edtName?.text.toString()
            birth = edtBirth?.text.toString()
            sex = spnSex?.selectedItem.toString()
            if (name?.isNotEmpty()!!) {
                lyt01?.visibility = View.GONE
                lyt02?.visibility = View.VISIBLE
            } else {
                Toast.makeText(this, "Complete the fields", Toast.LENGTH_SHORT).show()
            }
        }

        btnNext02?.setOnClickListener {
            skill01 = spnSkill1?.selectedItem.toString()
            skill02 = spnSkill2?.selectedItem.toString()
            skill03 = spnSkill3?.selectedItem.toString()
            lyt02?.visibility = View.GONE
            lyt03?.visibility = View.VISIBLE
        }

        btnNext03?.setOnClickListener {
            interest01 = spnInterest1?.selectedItem.toString()
            interest02 = spnInterest2?.selectedItem.toString()
            interest03 = spnInterest3?.selectedItem.toString()
            lyt03?.visibility = View.GONE
            lyt04?.visibility = View.VISIBLE
        }

        btnNext04?.setOnClickListener {
            bio = edtBio?.text.toString()
            lyt04?.visibility = View.GONE
            lyt05?.visibility = View.VISIBLE
        }

        btnRegister?.setOnClickListener {
            val email = edtEmail?.text.toString()
            val password = edtPassword?.text.toString()

            if (email.isNotEmpty()!! && password.isNotEmpty()!!) {
                firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener {
                    if (it.isSuccessful) {
                        Log.i("Register", "Successful")
                        val userId = firebaseAuth.currentUser?.uid

                        val currentUserDb = FirebaseDatabase.getInstance().reference.child("User").child(userId!!)
                        currentUserDb.child("Name").setValue(name)
                        currentUserDb.child("Bio").setValue(bio)
                        currentUserDb.child("Birth").setValue(birth)
                        currentUserDb.child("Email").setValue(email)
                        currentUserDb.child("Sex").setValue(sex)
                        currentUserDb.child("Interest1").setValue(interest01)
                        currentUserDb.child("Interest2").setValue(interest02)
                        currentUserDb.child("Interest3").setValue(interest03)
                        currentUserDb.child("Skill1").setValue(skill01)
                        currentUserDb.child("Skill2").setValue(skill02)
                        currentUserDb.child("Skill3").setValue(skill03)

                        val intent = Intent(this@Register, MainActivity::class.java)
                        startActivity(intent)
                    } else {
                        Log.i("Register", "Error")
                    }
                }
                    .addOnFailureListener {
                        Toast.makeText(this, it.toString(), Toast.LENGTH_SHORT).show()
                    }
            } else {
                Toast.makeText(this, "Complete the fields", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun renderButtons() {

        btnBack01 = findViewById(R.id.register_btn_back01)
        btnBack02 = findViewById(R.id.register_btn_back02)
        btnBack03 = findViewById(R.id.register_btn_back03)
        btnBack04 = findViewById(R.id.register_btn_back04)
        btnBack05 = findViewById(R.id.register_btn_back05)

        btnNext01 = findViewById(R.id.register_btn_next01)
        btnNext02 = findViewById(R.id.register_btn_next02)
        btnNext03 = findViewById(R.id.register_btn_next03)
        btnNext04 = findViewById(R.id.register_btn_next04)
        btnRegister = findViewById(R.id.register_btn_register)

    }

    private fun renderFields() {

        edtBirth = findViewById(R.id.register_edt_birth)
        edtName = findViewById(R.id.register_edt_name)
        spnSex = findViewById(R.id.register_spn_sex)

        spnSkill1 = findViewById(R.id.register_spn_skill1)
        spnSkill2 = findViewById(R.id.register_spn_skill2)
        spnSkill3 = findViewById(R.id.register_spn_skill3)

        spnInterest1 = findViewById(R.id.register_spn_interest1)
        spnInterest2 = findViewById(R.id.register_spn_interest2)
        spnInterest3 = findViewById(R.id.register_spn_interest3)

        edtBio = findViewById(R.id.register_edt_bio)

        edtEmail = findViewById(R.id.register_edt_email)
        edtPassword = findViewById(R.id.register_edt_password)
    }

    private fun renderLayouts() {
        lyt01 = findViewById(R.id.register_lyt_01)
        lyt02 = findViewById(R.id.register_lyt_02)
        lyt03 = findViewById(R.id.register_lyt_03)
        lyt04 = findViewById(R.id.register_lyt_04)
        lyt05 = findViewById(R.id.register_lyt_05)

        lyt01?.visibility = View.VISIBLE
        lyt02?.visibility = View.GONE
        lyt03?.visibility = View.GONE
        lyt04?.visibility = View.GONE
        lyt05?.visibility = View.GONE
    }
}
