package com.example.omadaapplication.Activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.omadaapplication.Cards.Cards
import com.example.omadaapplication.Cards.UserAdapter
import com.example.omadaapplication.Chat.Chat
import com.example.omadaapplication.Match.Matches
import com.example.omadaapplication.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.lorentzos.flingswipe.SwipeFlingAdapterView

class MainActivity : AppCompatActivity() {

    var btnLogout: Button? = null
    var navigation: BottomNavigationView? = null


    var card_data: MutableList<Cards>? = null
    var arrayAdapter: UserAdapter? = null
    var listView: ListView? = null
    var rowItems: MutableList<Cards>? = null

    var firebaseAuth: FirebaseAuth? = null
    var userDb : DatabaseReference? = null
    var currentUserId : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        firebaseAuth = FirebaseAuth(FirebaseApp.getInstance())
        firebaseAuth?.addAuthStateListener { user ->
            var auth = user.currentUser
            if(auth != null){
                currentUserId = auth.uid
            }else
            {

            }
        }

        navigation = findViewById(R.id.login_navigation)
        navigation?.selectedItemId = R.id.navigation_matchmaking
        navigation?.setOnNavigationItemSelectedListener { menuItem: MenuItem ->
            when (menuItem.itemId) {
                R.id.navigation_forum -> {
                    makeToast(this@MainActivity, "Feature in development stage")
                }
                R.id.navigation_matchmaking -> {}
                R.id.navigation_chat -> {
                    val intent = Intent(this@MainActivity, Matches::class.java)
                    var b = Bundle()
                    b.putString("currentUser", currentUserId)
                    intent.putExtras(b)
                    startActivity(intent)
                }
                else -> {}
            }
            return@setOnNavigationItemSelectedListener true
        }


        userDb = FirebaseDatabase.getInstance().reference.child("User")


        rowItems = ArrayList()

        arrayAdapter = UserAdapter(this, R.layout.item, rowItems!!)


        val flingContainer: SwipeFlingAdapterView? = findViewById(R.id.frame)


        flingContainer?.adapter = arrayAdapter as ArrayAdapter<String>
        flingContainer?.setFlingListener(object : SwipeFlingAdapterView.onFlingListener {
            override fun removeFirstObjectInAdapter() {
                Log.d("LIST", "removed object!")
                rowItems!!.removeAt(0)
                arrayAdapter!!.notifyDataSetChanged()
            }

            override fun onLeftCardExit(dataObject: Any) {
                var obj: Cards = dataObject as Cards
                var userId = obj._userId
                userDb?.child(userId)?.child("Connections")?.child("Nope")?.child(currentUserId!!)?.setValue(true)
                //makeToast(this@MainActivity, "Left!")
            }

            override fun onRightCardExit(dataObject: Any) {
                var obj: Cards = dataObject as Cards
                var userId = obj._userId
                userDb?.child(userId)?.child("Connections")?.child("Yep")?.child(currentUserId!!)?.setValue(true)
                isConnectionMatch(userId)
                //makeToast(this@MainActivity, "Right!")
            }

            override fun onAdapterAboutToEmpty(itemsInAdapter: Int) {
                // Ask for more data here
            }

            override fun onScroll(scrollProgressPercent: Float) {}
        })

        btnLogout = findViewById(R.id.main_btn_logout)
        btnLogout?.setOnClickListener {
            firebaseAuth?.signOut()
            val intent = Intent(this@MainActivity, Splash::class.java)
            startActivity(intent)
            finish()
        }

        getUsers()
    }

    private fun isConnectionMatch(userId: String) {
        var currentUserConnections = userDb?.child(currentUserId!!)?.child("Connections")?.child("Yep")?.child(userId)
        currentUserConnections?.addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                if(p0.exists()){
                    var key = FirebaseDatabase.getInstance().reference.child("Chat").push().key
                    userDb?.child(p0.key!!)?.child("Connections")?.child("Matches")?.child(currentUserId!!)?.child("ChatId")?.setValue(key)
                    userDb?.child(currentUserId!!)?.child("Connections")?.child("Matches")?.child(p0.key!!)?.child("ChatId")?.setValue(key)
                    makeToast(this@MainActivity, "Matched")
                }
            }

        })
    }

    fun makeToast(ctx: Context, s: String) {
        Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show()
    }

    private fun getUsers() {
        userDb?.addChildEventListener(
            object : ChildEventListener {

                override fun onChildAdded(dataSnapshot: DataSnapshot, p1: String?) {
                    if (dataSnapshot.exists() &&
                        dataSnapshot.key != currentUserId &&
                        !dataSnapshot.child("Connections").child("Nope").hasChild(currentUserId!!) &&
                        !dataSnapshot.child("Connections").child("Yep").hasChild(currentUserId!!)
                    ) {
                        var item = Cards(
                            dataSnapshot.key!!,
                            dataSnapshot.child("Name").value.toString(),
                            dataSnapshot.child("Bio").value.toString(),
                            dataSnapshot.child("Skill1").value.toString(),
                            dataSnapshot.child("Skill2").value.toString(),
                            dataSnapshot.child("Skill3").value.toString(),
                            dataSnapshot.child("Interest1").value.toString(),
                            dataSnapshot.child("Interest2").value.toString(),
                            dataSnapshot.child("Interest3").value.toString()
                        )
                        rowItems?.add(item)
                        arrayAdapter?.notifyDataSetChanged()
                    }
                }

                override fun onChildRemoved(dataSnapshot: DataSnapshot) {}

                override fun onCancelled(p0: DatabaseError) {}

                override fun onChildChanged(dataSnapshot: DataSnapshot, p1: String?) {}

                override fun onChildMoved(p0: DataSnapshot, p1: String?) {}

            })
    }
}
