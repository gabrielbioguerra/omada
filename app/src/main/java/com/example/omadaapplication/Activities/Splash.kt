package com.example.omadaapplication.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.omadaapplication.R
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth

class Splash : AppCompatActivity() {

    var btnRegister: Button? = null
    var btnLogin: TextView? = null

    var firebaseAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        firebaseAuth = FirebaseAuth(FirebaseApp.getInstance())
        firebaseAuth?.addAuthStateListener { user ->
            val user = user.currentUser
            if (user != null) {
                Log.d("User", "Signed in")
                val intent = Intent(this@Splash, MainActivity::class.java)
                startActivity(intent)
            } else {
                Log.d("User", "Signed out")
            }
        }

        btnRegister = findViewById(R.id.splash_btn_register)
        btnLogin = findViewById(R.id.splash_txt_login)

        btnRegister?.setOnClickListener {
            val intent = Intent(this@Splash, Register::class.java)
            startActivity(intent)
        }

        btnLogin?.setOnClickListener {
            val intent = Intent(this@Splash, Login::class.java)
            startActivity(intent)
        }
    }

    public override fun onStart() {
        super.onStart()
        firebaseAuth = FirebaseAuth(FirebaseApp.getInstance())
        firebaseAuth?.addAuthStateListener { user ->
            val user = user.currentUser
            if (user != null) {
                Log.d("User", "Signed in")
                val intent = Intent(this@Splash, MainActivity::class.java)
                startActivity(intent)
            } else {
                Log.d("User", "Signed out")
            }
        }
    }

    public override fun onPause() {
        super.onPause()
        firebaseAuth = FirebaseAuth(FirebaseApp.getInstance())
        firebaseAuth?.removeAuthStateListener { user ->
            val user = user.currentUser
            if (user != null) {
                Log.d("User", "Signed in")
                val intent = Intent(this@Splash, MainActivity::class.java)
                startActivity(intent)
            } else {
                Log.d("User", "Signed out")
            }
        }
    }
}
