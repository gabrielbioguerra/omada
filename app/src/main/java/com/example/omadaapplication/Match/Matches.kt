package com.example.omadaapplication.Match

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.omadaapplication.Activities.MainActivity
import com.example.omadaapplication.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*


class Matches : AppCompatActivity() {

    var recycler : RecyclerView? = null
    var navigation: BottomNavigationView? = null
    var back: Button?  = null

    var matchesAdapter: RecyclerView.Adapter<MatchesViewHolders>? = null
    var matchesLayoutManager: RecyclerView.LayoutManager? = null

    var resultsMatches = ArrayList<MatchObject>()

    var userDbMatches : DatabaseReference? = null
    var currentUserId : String? = null

    var firebaseAuth : FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_matches)
        currentUserId = intent.extras.getString("currentUser")

        userDbMatches = FirebaseDatabase.getInstance().reference.child("User").child(currentUserId!!).child("Connections").child("Matches")

        getUserMatchId()

        back = findViewById(R.id.match_btn_back)
        back?.setOnClickListener {
            onBackPressed()
        }

        recycler = findViewById(R.id.match_recycler)
        recycler?.isNestedScrollingEnabled = false
        recycler?.setHasFixedSize(true)
        matchesLayoutManager = LinearLayoutManager(this@Matches)
        recycler?.layoutManager = matchesLayoutManager
        matchesAdapter = MatchesAdapter(resultsMatches, this@Matches)
        recycler?.adapter = matchesAdapter
        matchesAdapter?.notifyDataSetChanged()





        navigation = findViewById(R.id.match_navigation)
        navigation?.selectedItemId = R.id.navigation_chat
        navigation?.setOnNavigationItemSelectedListener { menuItem: MenuItem ->
            when (menuItem.itemId) {
                R.id.navigation_forum -> {
                    Toast.makeText(this@Matches, "Feature in development stage", Toast.LENGTH_SHORT).show()
                }
                R.id.navigation_matchmaking -> {
                    val intent = Intent(this@Matches, MainActivity::class.java)
                    startActivity(intent)
                }
                R.id.navigation_chat -> {}
                else -> {}
            }
            return@setOnNavigationItemSelectedListener true
        }


    }

    private fun getUserMatchId() {
        userDbMatches?.addListenerForSingleValueEvent(object : ValueEventListener  {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                if(p0.exists()){
                    for(match in p0.children){
                        fetchMatchInformation(match.key)
                    }
                }
            }

        })
    }

    private fun fetchMatchInformation(key: String?) {
        var matchInfo = FirebaseDatabase.getInstance().reference.child("User").child(key!!)
        matchInfo.addListenerForSingleValueEvent(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                if(p0.exists()){
                    var name: String? = ""
                    var userId = p0.key

                    if(p0.child("Name").value != null){
                        name = p0.child("Name").value.toString()
                    }

                    var obj = MatchObject(userId!!, name!!)
                    resultsMatches.add(obj)
                    matchesAdapter?.notifyDataSetChanged()

                }
            }

        })
    }

    override fun onBackPressed() {
        navigation?.selectedItemId = R.id.navigation_matchmaking
    }
}
