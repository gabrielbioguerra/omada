package com.example.omadaapplication.Match

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.omadaapplication.Chat.Chat
import com.example.omadaapplication.R

class MatchesViewHolders(
    _view: View
) : RecyclerView.ViewHolder(_view), View.OnClickListener {

    var view = _view
    var view02 = _view.setOnClickListener(this)
    var holderName: TextView = view.findViewById(R.id.item_matches_name)
    var holderId: TextView = view.findViewById(R.id.item_matches_id)


    override fun onClick(v: View?) {
        val intent = Intent(view.context, Chat::class.java)
        var b = Bundle()
        b.putString("matchId", holderId.text.toString())
        intent.putExtras(b)
        view.context.startActivity(intent)
    }
}
