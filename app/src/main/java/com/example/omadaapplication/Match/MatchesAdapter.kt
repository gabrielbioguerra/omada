package com.example.omadaapplication.Match

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.omadaapplication.R

class MatchesAdapter(
    _matchesList: List<MatchObject>,
    _context: Context
) : RecyclerView.Adapter<MatchesViewHolders>() {

    var matchesList: List<MatchObject> = _matchesList
    var context: Context = _context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchesViewHolders {

        var layoutView = LayoutInflater.from(parent.context).inflate(R.layout.item_matches, null, false)
        var lp = RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layoutView.layoutParams = lp
        return MatchesViewHolders((layoutView))
    }

    override fun getItemCount(): Int {
        return matchesList.size
    }

    override fun onBindViewHolder(holder: MatchesViewHolders, position: Int) {
        holder.holderName.text = matchesList[position]._name
        holder.holderId.text = matchesList[position]._userId
    }
}